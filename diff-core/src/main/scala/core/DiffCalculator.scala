/** Provides classes for calculation diff of 2 [[Seq]].
  *
  * ==Overview==
  * The main class to use is [[core.StreamDiffCalculator]], as so
  * {{{
  * scala> val result = new core.StreamDiffCalculator().calculate(Seq(1,2,3), Seq(1,2,3))
  * result: core.DiffResult = Equals
  * }}}
  */
package core

/** Trait represents the result of comparison of 2 entities */
trait DiffResult

/** Case object represents result of comparison [[DiffResult]] when entities are equal */
case object Equals extends DiffResult

/** Object represents result of comparison [[DiffResult]] when entities are of the different sizes */
case object DifferentSize extends DiffResult

/** Case class that represents the result of comparison when entities has differences as a group of [[Diff]]
  *
  * @param diffs groups of differences [[Diff]]
  */
case class Difference(diffs: Seq[Diff]) extends DiffResult

/** Factory for [[Difference]] objects to simplify syntax */
object Differences {

  /** Creates a [[Difference]] with a [[Seq]] of [[Diff]]s from varargs tuples
    * {{{
    * scala> val dif = core.Differences((1,1),(2,2),(3,3))
    * dif: core.Difference = Difference(List(Diff(1,1), Diff(2,2), Diff(3,3)))
    * }}}
    *
    * @param diffs vararg pairs of [[Int]]
    * @return created object [[Difference]] with corresponding [[Diff]]s
    */
  def apply(diffs: (Int, Int)*): Difference = new Difference(diffs.toList.map(x => Diff(x._1, x._2)))
}

/** Class represents single difference group as tuple of offset in array and amount of different elements in this group
  *
  * @param offset starting position of this group of differences
  * @param length amount of different elements in this group
  */
case class Diff(offset: Int, length: Int = 1)

/** Trait of difference calculator between 2 [[T]] entities
  *
  * @tparam T type of entity to compare
  */
trait DiffCalculator[T] {

  /** Compare method
    *
    * @param left first entity
    * @param right second entity
    * @return result of comparison as [[DiffResult]]
    */
  def calculate(left: T, right: T): DiffResult
}

/** Implementation of diff calculator based on recursive approach
  *
  * @constructor create a new calculator
  * @tparam Y represents the type of sequences that will be processed
  */
class StreamDiffCalculator[Y] extends DiffCalculator[Seq[Y]] {

  /** Method that calculates the difference between 2 [[Seq]]
    *
    * @param left left sequence
    * @param right right sequence
    * @return [[core.Equals]] in case sequences are equal, [[core.DifferentSize]] in case sequences have different sizes
    *        [[core.Differences]] in case sequences have differences
    */
  override def calculate(left: Seq[Y], right: Seq[Y]): DiffResult =
    if (left.length != right.length) {
      DifferentSize
    } else {

      /** Recursive helper function for difference calculation
        *
        * @param array zipped [[Seq]] of pairs, first pair is active and processed, tail is passed to recursion
        * @param groups storage to keep aggregated group of differences [[core.Diff]]
        * @param lastGroup current group of differences [[Diff]], represented as [[Option]], if absent [[None]]
        * @param pos current position
        * @return [[Seq]] of processed groups of differences, that is not active
        */
      def calculateRec(array: Seq[(Y, Y)], groups: Seq[Diff], lastGroup: Option[Diff], pos: Int): Seq[Diff] = {
        def appendGroup: Seq[Diff] = if (lastGroup.isDefined) groups ++ lastGroup else groups
        array match {
          case Seq() => appendGroup
          case Seq((x, y), tail@_*) =>
            val (newLastGroup, newGroups) = if (x == y)
              (None, appendGroup)
            else if (lastGroup.isEmpty)
              (Some(Diff(pos)), groups)
            else
              (Some(Diff(lastGroup.get.offset, lastGroup.get.length + 1)), groups)
            calculateRec(tail, newGroups, newLastGroup, pos + 1)
        }
      }
      calculateRec(left.zip(right), Seq(), None, 0) match {
        case Seq() => Equals
        case x => Difference(x)
      }

    }

}

