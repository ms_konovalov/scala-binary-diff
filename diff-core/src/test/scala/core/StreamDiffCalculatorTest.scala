package core

import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{FreeSpec, Matchers}

class StreamDiffCalculatorTest extends FreeSpec with Matchers with TableDrivenPropertyChecks {

  val calc = new StreamDiffCalculator[Byte]

  "StreamDiffCalculator" - {

    val differentSize = Table(
      ( "left",
        "right"),
      ( Seq[Byte](1),
        Seq[Byte]()),
      ( Seq[Byte](1),
        Seq[Byte](1, 1)),
      ( Seq[Byte](1, 0, 6, 3, 3, 6),
        Seq[Byte](2))
    )

    "should return DifferentSize when arrays has different sizes" - {
      forAll(differentSize) { (left, right) =>
        val result = calc.calculate(left, right)
        result shouldBe DifferentSize
      }
    }

    val equals = Table(
      ( "left",
        "right"),
      ( Seq[Byte](),
        Seq[Byte]()),
      ( Seq[Byte](1),
        Seq[Byte](1)),
      ( Seq[Byte](1, 1),
        Seq[Byte](1, 1)),
      ( Seq[Byte](1, 0),
        Seq[Byte](1, 0)),
      ( Seq[Byte](1, 1, 0, 0, 1, 1, 0),
        Seq[Byte](1, 1, 0, 0, 1, 1, 0))
    )

    "should return Equals when arrays are equal" in {
      forAll(equals) { (left, right) =>
        val result = calc.calculate(left, right)
        result shouldBe Equals
      }
    }

    val nonEquals = Table(
      ( "left",
        "right",
        "expected"),
      ( Seq[Byte](1),
        Seq[Byte](0),
        Differences((0, 1))),
      ( Seq[Byte](1, 1, 0),
        Seq[Byte](0, 0, 1),
        Differences((0, 3))),
      ( Seq[Byte](1, 1, 0, 1, 1),
        Seq[Byte](0, 0, 1, 1, 1),
        Differences((0, 3))),
      ( Seq[Byte](1, 1, 0, 1, 1, 0, 1, 1, 0),
        Seq[Byte](0, 0, 1, 1, 1, 0, 0, 0, 0),
        Differences((0, 3), (6, 2))),
      ( Seq[Byte](1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
        Seq[Byte](0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1),
        Differences((0, 1), (2, 2), (5, 3), (9, 4))),
      ( Seq[Byte](1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
        Seq[Byte](0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1),
        Differences((0, 1), (2, 2), (5, 3), (9, 4)))
    )

    "should return Difference when arrays are not equal" in {
      forAll(nonEquals) { (left, right, expected) =>
        val result = calc.calculate(left, right)
        result should equal(expected)
      }
    }
  }
}
