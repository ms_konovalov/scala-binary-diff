package web

import akka.actor.{Actor, Props, Status}
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{ContentType, HttpEntity, MediaTypes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.parboiled2.util.Base64
import akka.testkit.TestActorRef
import core._
import org.scalamock.scalatest.MockFactory
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{Matchers, WordSpec}

class DiffRouteTest extends WordSpec with Matchers with ScalatestRouteTest with DiffProtocol
  with MockFactory with TableDrivenPropertyChecks {

  import spray.json._

  val probe = TestActorRef(Props(new DiffActor(mock[DiffCalculator[Seq[Byte]]])))
  val diffRoute = new DiffRoute(probe).route

  "The service" should {

    "return a NotFound error for GET requests to the root path" in {
      Get() ~> Route.seal(diffRoute) ~> check {
        status shouldEqual NotFound
      }
    }

    "return a MethodNotAllowed error for POST requests to the /v1/diff" in {
      Post("/v1/diff") ~> Route.seal(diffRoute) ~> check {
        status shouldEqual MethodNotAllowed
      }
    }

    "return a MethodNotAllowed error for GET requests to the /v1/diff/left" in {
      Get("/v1/diff/left") ~> Route.seal(diffRoute) ~> check {
        status shouldEqual MethodNotAllowed
      }
    }

    "return a MethodNotAllowed error for DELETE requests to the /v1/diff/right" in {
      Get("/v1/diff/right") ~> Route.seal(diffRoute) ~> check {
        status shouldEqual MethodNotAllowed
      }
    }

    "return BadRequest for GET requests to the /v1/diff if Left is empty" in {
      val diffRoute = new DiffRoute(createMockDiffActor(Status.Failure(new IllegalStateException("Left is not defined")))).route
      Get("/v1/diff") ~> diffRoute ~> check {
        status shouldEqual BadRequest
        responseAs[Error] shouldEqual Error("Left is not defined")
      }
    }

    val results = Table(
      "value",
      Equals,
      DifferentSize,
      Differences((1, 1)),
      Differences((1, 1), (1, 3), (5, 7))
    )

    "return Success for GET requests to the /v1/diff" in {
      forAll(results) { value =>
        val diffRoute = new DiffRoute(createMockDiffActor(DiffResponse(value))).route
        Get("/v1/diff") ~> diffRoute ~> check {
          status shouldEqual OK
          responseEntity shouldEqual HttpEntity(ContentType(MediaTypes.`application/json`),
            value.asInstanceOf[DiffResult].toJson.prettyPrint)
        }
      }
    }

    "return Accepted for POST requests to the /v1/diff/left with correct Data" in {
      Post("/v1/diff/left", RequestData(Seq[Byte](1, 2, 3, 4, 5, 6, 7))) ~> diffRoute ~> check {
        status shouldEqual Accepted
      }
    }

    "return UnsupportedMediaType for POST requests to the /v1/diff/left with text/plain" in {
      Post("/v1/diff/left", "some data") ~> Route.seal(diffRoute) ~> check {
        status shouldEqual UnsupportedMediaType
      }
    }

    "return BadRequest for POST requests to the /v1/diff/left with wrong json" in {
      Post("/v1/diff/left", HttpEntity(ContentType(MediaTypes.`application/json`), """{"some" : "data"}""")) ~>
        Route.seal(diffRoute) ~> check {
        status shouldEqual BadRequest
      }
    }

    "return InternalServerError for POST requests to the /v1/diff/left if something went wrong" in {
      val diffRoute = new DiffRoute(createMockDiffActor(Status.Failure(new RuntimeException()))).route
      Post("/v1/diff/left", RequestData(Seq[Byte](1, 2, 3, 4, 5, 6, 7))) ~> diffRoute ~> check {
        status shouldEqual InternalServerError
        responseAs[Error] shouldEqual Error("Operation failed")
      }
    }
  }

  private def createMockDiffActor(response: Any) = {
    TestActorRef(new Actor {
      def receive = {
        case _ => sender() ! response
      }
    })
  }

}