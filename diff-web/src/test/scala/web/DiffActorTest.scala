package web

import akka.actor.Status.Failure
import akka.actor.{ActorRef, ActorSystem, Status}
import akka.testkit.{ImplicitSender, TestKit}
import core.{DiffCalculator, Equals}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class DiffActorTest(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll with MockFactory {

  val diffCalculator = mock[DiffCalculator[Seq[Byte]]]
  val data: Seq[Byte] = Seq[Byte](1, 2, 3)

  def this() = this(ActorSystem("MySpec"))

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "DiffActor actor" must {

    "successfully set Left" in {
      val diffActor = system.actorOf(DiffActor.prop(diffCalculator))
      setLeft(diffActor)
    }

    "successfully set Right" in {
      val diffActor = system.actorOf(DiffActor.prop(diffCalculator))
      setRight(diffActor)
    }

    "successfully set Right and Left" in {
      val diffActor = system.actorOf(DiffActor.prop(diffCalculator))
      setRight(diffActor)
      setLeft(diffActor)
    }

    "fail to get DiffResult without Right and Left" in {
      val diffActor = system.actorOf(DiffActor.prop(diffCalculator))
      getDiff(diffActor, classOf[Status.Failure])
    }

    "fail to get DiffResult without Left" in {
      val diffActor = system.actorOf(DiffActor.prop(diffCalculator))
      setRight(diffActor)
      getDiff(diffActor, classOf[Failure])
    }

    "fail to get DiffResult without Right" in {
      val diffActor = system.actorOf(DiffActor.prop(diffCalculator))
      setLeft(diffActor)
      getDiff(diffActor, classOf[Failure])
    }

    "successfully get DiffResult with Right and Left" in {
      (diffCalculator.calculate _).expects(data, data).noMoreThanOnce().returns(Equals)
      val diffActor = system.actorOf(DiffActor.prop(diffCalculator))
      setLeft(diffActor)
      setRight(diffActor)
      getDiff(diffActor, classOf[DiffResponse])
    }

    "successfully set and get DiffResult, Right and Left several times" in {
      (diffCalculator.calculate _).expects(data, data).anyNumberOfTimes().returns(Equals)
      val diffActor = system.actorOf(DiffActor.prop(diffCalculator))
      setLeft(diffActor)
      setLeft(diffActor)
      setRight(diffActor)
      getDiff(diffActor, classOf[DiffResponse])
      setRight(diffActor)
      getDiff(diffActor, classOf[DiffResponse])
      getDiff(diffActor, classOf[DiffResponse])
    }

  }

  def setLeft(diffActor: ActorRef): Unit = {
    diffActor ! SetLeftRequest(data)
    expectMsg(Status.Success)
  }

  def setRight(diffActor: ActorRef): Unit = {
    diffActor ! SetRightRequest(data)
    expectMsg(Status.Success)
  }

  def getDiff[T](diffActor: ActorRef, result: Class[T]): Unit = {
    diffActor ! GetDiffRequest
    expectMsgClass(result)
  }
}
