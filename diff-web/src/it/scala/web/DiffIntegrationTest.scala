package web

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{HttpEntity, _}
import akka.stream.ActorMaterializer
import akka.testkit.{ImplicitSender, TestKit}
import core.{DiffResult, Differences, DifferentSize, Equals}
import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.{PatienceConfiguration, ScalaFutures}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, Matchers, WordSpecLike}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.language.postfixOps

/** Integration test - can be run manually or with sbt
  *
  * Default values
  *
  * {{{
  * host = localhost
  * port = 8080
  * }}}
  *
  * can be set with
  *
  * {{{
  *   -Dit.host=localhost -Dit.port=8080
  * }}}
  */
class DiffIntegrationTest(_system: ActorSystem) extends TestKit(_system) with ImplicitSender with DiffProtocol
  with WordSpecLike with Matchers with BeforeAndAfterAll with BeforeAndAfterEach with MockFactory {

  import spray.json._

  implicit val materializer = ActorMaterializer()
  implicit val ec = ExecutionContext.Implicits.global
  implicit val timeout: PatienceConfiguration.Timeout = Timeout(5 seconds)

  private val host = sys.props.get("it.host").getOrElse("localhost")
  private val port = sys.props.get("it.port").getOrElse("8080").toInt

  private val leftUrl = s"http://$host:$port/v1/diff/left"
  private val rightUrl = s"http://$host:$port/v1/diff/right"
  private val diffUrl = s"http://$host:$port/v1/diff"
  private val data = RequestData(Seq[Byte](1, 2, 3, 4, 5, 6, 7))
  private val data2 = RequestData(Seq[Byte](1, 2, 3, 4, 5, 6, 7, 8, 9, 0))
  private val data3 = RequestData(Seq[Byte](1, 2, 4, 3, 6, 6, 7, 8, 0, 1))

  def this() = this(ActorSystem("MySpec"))

  override def beforeEach = {
    reset(leftUrl)
    reset(rightUrl)
  }

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  private def diff[U](fun: HttpResponse => U) = {
    call(diffUrl, HttpMethods.GET) { response =>
      fun(response)
    }
  }

  private def reset(url: String) = {
    call(url, HttpMethods.DELETE) { response =>
      response.status shouldBe NoContent
    }
  }

  private def set[U](url: String, data: RequestData)(fun: HttpResponse => U) = {
    call(url, HttpMethods.POST, Some(data)) { response =>
      fun(response)
    }
  }

  private def call[T, U](url: String, method: HttpMethod, entity: Option[RequestData] = None)(fun: HttpResponse => U)(implicit timeout: PatienceConfiguration.Timeout) = {
    val value = entity.map(value => Await.result(Marshal(value).to[RequestEntity], timeout.value)).getOrElse(HttpEntity.Empty)
    ScalaFutures.whenReady(Http().singleRequest(
      HttpRequest(uri = url, method = method, entity = value)), timeout = timeout) { response =>
      system.log.info(s"Calling ${method.value} for $url ends with ${response.status}")
      fun(response)
    }
  }

  private def setLeft(data: RequestData): Unit = {
    set(leftUrl, data) { response =>
      response.status shouldBe Accepted
    }
  }

  private def setRight(data: RequestData): Unit = {
    set(rightUrl, data) { response =>
      response.status shouldBe Accepted
    }
  }

  private def toEntity(value: DiffResult): HttpEntity = {
    HttpEntity(ContentType(MediaTypes.`application/json`), value.toJson.prettyPrint)
  }

  "Client" must {

    "receive BadRequest when calling GET without setting Left and Right" in {
      diff { response =>
        response.status shouldBe BadRequest
      }
    }

    "receive BadRequest when calling GET after setting only Left" in {
      setLeft(data)
      diff { response =>
        response.status shouldBe BadRequest
      }
    }

    "successfully set left" in {
      setLeft(data)
    }

    "successfully set right" in {
      setRight(data)
    }

    "successfully set left and right" in {
      setLeft(data)
      setRight(data)
    }

    "successfully get diff after set left and right with the same data" in {
      setLeft(data)
      setRight(data)
      diff { response =>
        response.status shouldBe OK
        response.entity shouldBe toEntity(Equals)
      }
    }

    "successfully get diff after set left and right with different size data" in {
      setLeft(data)
      setRight(data2)
      diff { response =>
        response.status shouldBe OK
        response.entity shouldBe toEntity(DifferentSize)
      }
    }

    "successfully get diff after set left and right with different data" in {
      setLeft(data3)
      setRight(data2)
      diff { response =>
        response.status shouldBe OK
        response.entity shouldBe toEntity(Differences((2,3),(8,2)))
      }
    }
  }

}
