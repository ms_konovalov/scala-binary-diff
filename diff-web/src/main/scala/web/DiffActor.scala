package web

import akka.actor.{Actor, ActorLogging, Props, Status}
import core.{DiffCalculator, DiffResult}

object DiffActor {
  def prop(diffCalculator: DiffCalculator[Seq[Byte]]): Props = Props(classOf[DiffActor], diffCalculator)
}

/** Actor that store data for comparison and provide comparison result
  * It processed [[SetLeftRequest]], [[SetRightRequest]] and [[GetDiffRequest]]
  * Responds with [[Status.Success]] if [[SetLeftRequest]] or [[SetRightRequest]] completes successfully
  * Send [[DiffResponse]] as a response for [[GetDiffRequest]] if calculation finished successfully
  * Also can respond with [[Status.Failure]] in case of errors, e.g. if one of compared items is missed
  *
  * @param diffCalculator service to calculate differences
  */
class DiffActor(diffCalculator: DiffCalculator[Seq[Byte]]) extends Actor with ActorLogging {

  private var left: Option[Seq[Byte]] = None
  private var right: Option[Seq[Byte]] = None

  override def receive: Receive = {

    case SetLeftRequest(data) =>
      left = Some(data)
      sender() ! Status.Success

    case SetRightRequest(data) =>
      right = Some(data)
      sender() ! Status.Success

    case GetDiffRequest =>
      sender() ! calculateDiff

    case ResetLeft =>
      left = None
      sender() ! Status.Success

    case ResetRight =>
      right = None
      sender() ! Status.Success
  }

  private def calculateDiff = {
    if (left.isEmpty)
      Status.Failure(new IllegalStateException("Left is not defined"))
    else if (right.isEmpty)
      Status.Failure(new IllegalStateException("Right is no defined"))
    else
      DiffResponse(diffCalculator.calculate(left.get, right.get))
  }
}

/** Message for request store data for 1st cell
  *
  * @param data [[Seq]] of [[Byte]] for comparison
  */
case class SetLeftRequest(data: Seq[Byte])

/** Message for request store data for 2st cell
  *
  * @param data [[Seq]] of [[Byte]] for comparison
  */
case class SetRightRequest(data: Seq[Byte])

/** Message for request comparison result */
case object GetDiffRequest

/** Message for response the result of comparison
  *
  * @param result result of comparison
  */
case class DiffResponse(result: DiffResult)

/** Message to reset Left to [[None]] */
case object ResetLeft

/** Message to reset Right to [[None]] */
case object ResetRight
