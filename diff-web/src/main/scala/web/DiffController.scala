package web

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.marshalling.ToResponseMarshallable.apply
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{HttpResponse, ResponseEntity, StatusCode}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.ExceptionHandler
import akka.pattern.{AskTimeoutException, ask}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import core.StreamDiffCalculator

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.io.StdIn
import scala.language.postfixOps

/** Http Route for client requests
  *
  * @param diffActor actor [[DiffActor]] that provide diff calculation functionality
  */
class DiffRoute(diffActor: ActorRef)
               (implicit am: ActorMaterializer, ec: ExecutionContext)
  extends DiffProtocol {

  implicit val timeout = Timeout(5 seconds)

  val exceptionHandler = ExceptionHandler {
    case e: IllegalStateException =>
      convertError(BadRequest, Error(e.getMessage))
    case e: AskTimeoutException =>
      convertError(InternalServerError, Error("Operation timeout"))
    case e =>
      convertError(InternalServerError, Error(s"Operation failed"))
  }

  val route =
    handleExceptions(exceptionHandler) {
      pathPrefix("v1" / "diff") {
        pathEnd {
          get {
            logRequest("GET-DIFF") {
              onComplete((diffActor ? GetDiffRequest).mapTo[DiffResponse]) { result =>
                complete(result.get.result)
              }
            }
          }
        } ~
          path("left") {
            post {
              entity(as[RequestData]) { data =>
                logRequest("POST-LEFT") {
                  onComplete(diffActor ? SetLeftRequest(data.data)) { result =>
                    complete(result.map(_ => HttpResponse(Accepted)).get)
                  }
                }
              }
            } ~
            delete {
              logRequest("DELETE-LEFT") {
                onComplete(diffActor ? ResetLeft) { result =>
                  complete(result.map(_ => HttpResponse(NoContent)).get)
                }
              }
            }
          } ~
          path("right") {
            post {
              entity(as[RequestData]) { data =>
                logRequest("POST-RIGHT") {
                  onComplete(diffActor ? SetRightRequest(data.data)) { result =>
                    complete(result.map(_ => HttpResponse(Accepted)).get)
                  }
                }
              }
            } ~
            delete {
              logRequest("DELETE-RIGHT") {
                onComplete(diffActor ? ResetRight) { result =>
                  complete(result.map(_ => HttpResponse(NoContent)).get)
                }
              }
            }
          }
      }
    }

  /** Helper function to convert [[Error]] to [[HttpResponse]]
    *
    * @param status status code
    * @param error  error message
    * @return error representation
    */
  def convertError(status: StatusCode, error: Error) = {
    onSuccess(Marshal(error).to[ResponseEntity]) { entity =>
      complete {
        HttpResponse(status = status, entity = entity)
      }
    }
  }
}

/** Class represents request data for comparison converted from Json
  *
  * @param data comparison data
  */
case class RequestData(data: Seq[Byte])

/** Class represents error response converted to Json
  *
  * @param error error message
  */
case class Error(error: String)


object Main extends App {

  implicit val system = ActorSystem("my-system")
  implicit val materializer = ActorMaterializer()
  implicit val ec = system.dispatcher

  val host = if (args.length > 0 && args(0) != null) args(0) else "localhost"
  val port = if (args.length > 1 && args(1) != null) args(1).toInt else 8080

  val bindingFuture = Http().bindAndHandle(
    new DiffRoute(system.actorOf(DiffActor.prop(new StreamDiffCalculator[Byte]))).route, host, port)

  println(s"Diff server started on interface $host and port $port")
  println("Press any key to exit...")

  StdIn.readLine()

  bindingFuture.flatMap(_.unbind()).onComplete(_ => system.terminate())
}

