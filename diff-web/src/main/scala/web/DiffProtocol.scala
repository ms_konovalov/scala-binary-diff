package web

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller, ToResponseMarshaller}
import akka.http.scaladsl.model.{ContentType, HttpEntity, HttpResponse, MediaTypes}
import akka.http.scaladsl.unmarshalling.{FromResponseUnmarshaller, Unmarshaller}
import akka.parboiled2.util.Base64
import core._
import spray.json.{DefaultJsonProtocol, DeserializationException}

/** Trait contains converters to Json for domain model [[core.DiffResult]] */
trait DiffProtocol extends SprayJsonSupport with DefaultJsonProtocol {

  import spray.json._

  /** Implicit converter for [[Diff]] to Json format */
  implicit val diffFormat = jsonFormat2(Diff)

  /** Implicit converter for [[DiffResult]] to Json format */
  implicit val diffResultFormat = new RootJsonWriter[DiffResult] {

    override def write(result: DiffResult): JsValue = result match {
      case Equals => JsObject(
        Tuple2("result", JsString("Equals"))
      )

      case DifferentSize => JsObject(
        Tuple2("result", JsString("Different Size"))
      )

      case Difference(diffs) => JsObject(
        Tuple2("result", JsString("Different")),
        Tuple2("diff", diffs.toJson)
      )
    }
  }

  /** Implicit marshaller from Json format for client requests
    *
    * {{{
    *   {
    *     "data" : "lkjhlJHkjhgKjhgKJjyty7Ghkj"
    *   }
    * }}}
    * In case of format incorrectness throws [[DeserializationException]]
    */
  implicit val dataRequestFormat = new RootJsonFormat[RequestData] {

    override def read(value: JsValue): RequestData = {
      val data = value.asJsObject.getFields("data")
      if (data.size != 1 || !data.head.isInstanceOf[JsString])
        deserializationError("Ambiguous data")
      RequestData(Base64.rfc2045.decodeFast(data.head.toString()).toSeq)
    }

    override def write(data: RequestData): JsValue = {
      JsObject(
        Tuple2("data", JsString(Base64.rfc2045.encodeToString(data.data.toArray, false)))
      )
    }
  }

  /** Implicit converter to Json format for [[Error]] error messages */
  implicit val errorFormat = jsonFormat1(Error)

  implicit def errorResponseMarshaller: ToEntityMarshaller[Error] =
    Marshaller.withFixedContentType(MediaTypes.`application/json`) { error =>
      HttpEntity(ContentType(MediaTypes.`application/json`), error.toJson.compactPrint)
    }
}
