Diff calculation REST service
=============================

Based on scala and akka it provides ability to send 2 pieces of biunary data and then compare it and find difference between them.

It provides 2 endpoints http://{host}:{port}/v1/diff/left and http://{host}:{port}/v1/diff/right to POST data to compare in Json format.

        {
          "data" : "<some_base64_data>"
        }

The diff between 2 pieces of data is available on http://{host}:{port}/v1/diff with GET request. 
In case one of pieces of data is absent it will return

        Status Code: 400 Bad Request
        Content-Type: application/json
        {
          "error" : "Left is not defined"
        }
    
Otherwise the following responses can occur for different input data

__for equal:__

        Status Code: 200 OK
        Content-Type: application/json
        {
          "result": "Equals"
        }
    
__for different size:__
    
        Status Code: 200 OK
        Content-Type: application/json
        {
          "result": "Different Size"
        }

__for different data:__

        Status Code: 200 OK
        Content-Type: application/json
        {
          "result": "Different",
          "diff": [{
            "offset": 1,
            "length": 2
          }, {
            "offset": 12,
            "length": 3
          }]
        }
        
Also for test purposes it provides 2 additional endpoints http://{host}:{port}/v1/diff/left and http://{host}:{port}/v1/diff/right with DELETE method to reset state of stored data
        
### Starting ###

To compile and start application go to root directory and 
 
    % sbt package
    
    [info] Loading project definition from /.../binary-diff/project
    [info] Set current project to binary-diff (in build file:/.../binary-diff/)
    [info] Compiling ... Scala source to /.../binary-diff/diff-web/target/scala-2.11/classes...
    [info] Packaging /.../binary-diff/diff-web/target/scala-2.11/diff-web_2.11-1.0.jar ...
    [info] Done packaging.
    [success] Total time: 7 s, completed Mar 25, 2016 5:09:35 PM
    
    
    % sbt sbt 'project diff-web' "run 127.0.0.1 8899"
    
    [info] Loading project definition from /.../binary-diff/project
    [info] Set current project to binary-diff (in build file:/.../binary-diff/)
    [info] Set current project to diff-web (in build file:/.../binary-diff/)
    [info] Running web.Main 127.0.0.1 8899
    Diff server started on interface 127.0.0.1 and port 8899
    Press any key to exit...

### Integration testing ###

Project contains integration test which can be started manually with ScalaTest or via SBT

    % sbt it:test

Default values

    host = localhost
    port = 8080
 
can be set with
  
    -Dit.host=<host> -Dit.port=<port>
  
  