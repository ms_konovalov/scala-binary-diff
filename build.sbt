val commonSettings = Seq(
  version := "1.0",
  scalaVersion := "2.11.8"
)

lazy val root = (project in file("."))
  .settings(
    name := """binary-diff""",
    commonSettings
  ).aggregate(`diff-core`, `diff-web`)

lazy val `diff-core` = (project in file("diff-core"))
  .configs(IntegrationTest)
  .settings(
    name := "diff-core",
    libraryDependencies ++= Dependencies.core,
    dependencyOverrides ++= Dependencies.overrides,
    parallelExecution in Test := false,
    Testing.settings,
    commonSettings
  )

lazy val `diff-web` = (project in file("diff-web"))
  .configs(IntegrationTest)
  .settings(
    name := "diff-web",
    libraryDependencies ++= Dependencies.web,
    dependencyOverrides ++= Dependencies.overrides,
    parallelExecution in Test := false,
    Testing.settings,
    commonSettings
  ).dependsOn(`diff-core`)

mainClass in (Compile, run) := Some("web.Main")


